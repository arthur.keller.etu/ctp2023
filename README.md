# R3.04, Qualité de Développement, Contrôle TP du 13 décembre 2023

*Conseil : lire en entier avant de démarrer (c'est pas bien long !)*

## Tirage au sort des cadeaux

Ho, ho, ho !

Il s'agit pour ce CTP de réaliser un logiciel basique pour réaliser un tirage au sort de cadeaux (Noël approche). Le principe est simple, on entre les prénoms des participants et le logiciel indique qui donne un cadeau à qui. 

Voici une capture vidéo du logiciel attendu :

<a>![](secret_santa.gif)</a>

Pour se simplifier la tâche, le résultat sera toujours circulaire. Cela signifie par exemple que Rodolphe donne à quelqu'un, qui donne à quelqu'un d'autre, ainsi de suite jusqu'à arriver à quelqu'un qui donne à Rodolphe.

On remarquera : 

- qu'on peut ajouter des prénoms en faisant entrée dans le champ de saisie ou en appuyant sur le bouton **+** mais que si un prénom est déjà présent, il n'est pas ajouté une seconde fois.
- que tout nouveau tirage prend en compte l'ensemble des participants ajoutés 
- qu'on peut relancer le tirage autant de fois qu'on le souhaite (sans tenir compte du résultat de précédents tirages)

## Attendus

Le résultat de votre travail devra être disponible sur un dépôt git *ctp-2023* sur le groupe déjà existant qui vous correspond sur https://gitlab.univ-lille.fr/2023-iut-info-r3.04/groupe-x/nom-prenom , en remplaçant x par la lettre de votre groupe et nom-prenom par les vôtres.  

<a>![](boucle_tdd.png)</a>

- Le **modèle** sera à faire **en TDD** (commit à chaque étape, création du test compris), sauf la partie aléatoire.
- L'**architecture** devra respecter le patron **MVC**.
- L'**ensemble du code** devra être aussi *propre* que possible (**clean code**), vous pouvez recourir à des outils d'analyse statique.
- Votre **dépôt** devra être propre et structuré, n'oubliez pas de *push* !

## Ressources

Durant ce CTP, vous pouvez consultez vos autres TP, la documentation, le cours et utiliser votre IDE avec ses extensions.
Interdiction formelle de communication ou de transfert de fichiers sous peine de sanction importante.
À noter que tout départ avant la fin du temps imparti devra être signalé à l'enseignant présent.

Un fichier `pom.xml` est fourni pour qui aurait l'habitude d'utiliser maven, pour les autres, vous pouvez simplement le supprimer si cela vous perturbe. :warning: Si vous souhaitez le supprimer, attention de bien le supprimer avant d'ouvrir le projet avec eclipse ou vscode.

Enfin, voici une classe d'exemple contenant le code javaFX qui pourra vous servir pour votre vue (on ne cherche pas ici à évaluer vos compétences en javaFX). Bien entendu, il s'agit d'adapter et refactoriser ce code comme il convient dans votre logiciel.

```java
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SampleView extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        VBox left = new VBox();
        Button addBtn = new Button();
        Label title = new Label("Inscrits :");
        TextField input = new TextField();
        input.setMinSize(10, 30);
        addBtn.setText("+");
        left.getChildren().addAll(title, input, addBtn);

        VBox right = new VBox();
        Button btn = new Button();
        btn.setText("Nouveau tirage !");
        Label circularTitle = new Label("Affichage circulaire");
        TextArea circularContent = new TextArea();
        circularContent.appendText("Example1");
        circularContent.appendText("\n -> Example2");
        circularContent.setPrefSize(250,100);
        Label alphaTitle = new Label("Affichage alphabétique");
        TextArea alphaContent = new TextArea();
        alphaContent.setPrefSize(250,100);
        alphaContent.appendText("Example1 -> Example2");
        right.getChildren().addAll(btn, circularTitle, circularContent, alphaTitle, alphaContent);

        HBox root = new HBox(left, right);
        Scene scene = new Scene(root, 400, 400);
        primaryStage.setTitle("Tirage au sort des cadeaux");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
```
